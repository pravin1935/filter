import React  , {Component} from 'react';

export default class Searchfilter extends Component {

	constructor(props){
		super(props);
		this.state = {
			filterinput:""
		}

		this.handlechange = this.handlechange.bind(this);
	}


	handlechange(event){
		this.setstate({ filterinput: event.target.value});
	}

	render(){
		return(

			

				<input type="text"  className="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg" value={this.props.filterdata} onChange = {(event)=> this.props.onchange(event.target.value)} />

			

			);
	}
}