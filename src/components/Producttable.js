import React, { Component } from 'react';

export default class Producttable extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        var searchdata = this.props.filtertext;
        var products = this.props.products;
        console.log(products[0]);
        var ischecked = this.props.instockonly;
        var row = [];
        products.forEach(
            (product , index) => {

            	// searchdata = new RegExp('searchdata' , 'ig');
                if (product.name.indexOf(searchdata) ) {
                    
                    console.log(product.name);
                    console.log(searchdata);
                    return;
                } else if (!product.stocked && ischecked) {
                	console.log("not in stock");
                    return;
                } else {

                    var trstyle = '';

                    if (product.stocked === false) {
                        trstyle = {
                            color: "red"
                        };
                    }

                    else {
                        
                        trstyle = {
                            color: "inherit"
                        };
                    }

                    const row_header = <tr key={products.category}>
										<th scope="row" >
									{product.category}
										</th>
										</tr>;

					row.push(row_header);

                    const row_data = <tr key={products.name}>
										<td style={trstyle}>
									{product.name}
										</td>
										<td>
									{product.price}
										</td>
									</tr>;

					row.push(row_data);
                }


            });

        return (

            <div >
            <table className = "table table-dark">
            <thead>
			    <tr>
			      <th scope="col">Name</th>
			      <th scope="col">Price</th>
			    </tr>
			  </thead>
            <tbody>
				{row}
				</tbody>
			</table>
			</div>


        );
    }
}