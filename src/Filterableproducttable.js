import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Searchfilter from './components/Searchfilter';
import Checkbox from './components/Checkbox';
import Producttable from './components/Producttable';


export default class Filterableproducttable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterinput: "",
            ischecked: true
        }

        this.onchangeinput = this.onchangeinput.bind(this);
        this.onchangeclick = this.onchangeclick.bind(this);

    }

    onchangeinput(data) {
        this.setState({ filterinput: data });
    }

    onchangeclick() {
        this.setState({ ischecked: !this.state.ischecked });
    }


    render() {

        var sample = this.props.products;
        var a = [1, 2, 3, 4];
        return (
            <div className="col-sm-8 align-items-center">
      <h1></h1>
      <div className="input-group mb-3">
        <div className="input-group-prepend">
          <div className="input-group-text">
             <Checkbox onclick = {this.onchangeclick} checkedvalue = {this.state.ischecked} />
          </div>
        </div>
              <Searchfilter onchange = {this.onchangeinput} filterdata = {this.state.filterinput}/>
        </div>

      <Producttable products= {this.props.products} instockonly = {this.state.ischecked} filtertext = {this.state.filterinput} />
      
      </div>
        );
    }

}

// export default App;